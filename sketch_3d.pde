float k=0;
int speed=1;
int dist=85;

void setup(){
  size(800,600, P3D);        //ukuran layout
}

void draw(){
  background(255);                    //warna putih
  translate(width/2, height/2,0);      //meletakkan object ditengah-tengah
  scale(2);
  stroke(0);                      //garis sisi kubus berwarna hitam
  line(-80,0,0,80,0,0);        //membuat garis horizontal
  line(0,-80,0,0,80,0);        //membuat garis vartikal
  
  draw_cube(0*dist,0);      //koordinat kubus pada matrix baru
  
  k=k+(0.05*speed);        //kecepatan rotasi kubus
}

void draw_cube(float beg_x, int rotate_xyz){
  fill(#daeaf6);        //warna kubus
  pushMatrix();         
  if (keyPressed){
  if(key == 'a'|| key =='A') {           //pitch
    translate(beg_x,0,0);
    rotateX(k);
  }
  if (key =='s' || key == 'S') {          //roll
    translate(beg_x,0,0);                
    rotateX(-0.2);
    rotateY(k);
  }
  if (key == 'd' || key == 'D') {        //yaw
    translate(beg_x,0,0);                
    rotateY(0.2);
    rotateZ(k);
  }
  }
  box(40);          //ukuran kubus/kotak
  popMatrix();
}
