int x = 30;      //koordinat x awal text
int y = 30;      //koordinat y awal text
float s = 1;
int arah = 1;

void setup() {
  size(800, 600);          //ukuran layout
  textSize(20);            //ukuran awal text
  
}

void draw(){
  background(#daeaf6);          //memberi warna pada background
  fill(0);              //warna text
  translate(x,y);              //untuk pergeseran koordinat objek
  scale(s);                    //perkecil/perbesar koordinat dari object
  
 
  if (keyPressed) {
    
    if (key == 'S' || key == 's') {        //set key untuk pergerakan object ke bawah
      y = y+arah;
    }
    
    if (key == 'W' || key == 'w') {        //set key untuk pergerakan object  ke atas
      y = y-arah;
    } 
    
    if (key == 'D' || key == 'd') {      //set key untuk pergerakan object ke kanan
      x = x+arah;
    }
    if (key == 'A' || key == 'a') {      //set key untuk movement object to left
      x = x-arah;
    }
    if (key == 'Z' || key == 'z'){      //set key untuk zoom in object
      s = s+1;
    }
    if (key == 'C' || key == 'c'){      //set key untuk zoom out object
      s = s-0.5;
    }
  }
  
  
   text("DP035",0,0);
  }
